<?php
/**
 * Genesis Child.
 *
 * This file adds functions to the Genesis Child Theme.
 *
 * @package Genesis Child
 * @author  StudioPress
 * @license GPL-2.0+
 * @link    http://www.studiopress.com/
 */

// Start the engine.
include_once( get_template_directory() . '/lib/init.php' );

// Setup Theme.
include_once( get_stylesheet_directory() . '/lib/theme-defaults.php' );

// Set Localization (do not remove).
add_action( 'after_setup_theme', 'genesis_child_localization_setup' );
function genesis_child_localization_setup(){
	load_child_theme_textdomain( 'genesis-child', get_stylesheet_directory() . '/languages' );
}

// Add the helper functions.
include_once( get_stylesheet_directory() . '/lib/helper-functions.php' );

// Add Image upload and Color select to WordPress Theme Customizer.
require_once( get_stylesheet_directory() . '/lib/customize.php' );

// Include Customizer CSS.
include_once( get_stylesheet_directory() . '/lib/output.php' );

// Add WooCommerce support.
//include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-setup.php' );

// Add the required WooCommerce styles and Customizer CSS.
//include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-output.php' );

// Add the Genesis Connect WooCommerce notice.
//include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-notice.php' );

// Child theme (do not remove).
define( 'CHILD_THEME_NAME', 'Genesis Child' );
define( 'CHILD_THEME_URL', 'http://www.studiopress.com/' );
define( 'CHILD_THEME_VERSION', '2.3.3' );

// Enqueue Scripts and Styles.
add_action( 'wp_enqueue_scripts', 'genesis_child_enqueue_scripts_styles' );
function genesis_child_enqueue_scripts_styles() {

	wp_enqueue_style( 'genesis-child-fonts', '//fonts.googleapis.com/css?family=Lato:400,700|Open+Sans:300,300i,400,600,700', array(), CHILD_THEME_VERSION );
	wp_enqueue_style( 'dashicons' );

	// Custom CSS
    wp_enqueue_style( 'genesis-child-custom-style', get_stylesheet_directory_uri() . '/custom.css', array(), CHILD_THEME_VERSION, 'all' );

	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
	wp_enqueue_script( 'genesis-child-responsive-menu', get_stylesheet_directory_uri() . "/js/responsive-menus{$suffix}.js", array( 'jquery' ), CHILD_THEME_VERSION, true );
	wp_localize_script(
		'genesis-child-responsive-menu',
		'genesis_responsive_menu',
		genesis_child_responsive_menu_settings()
	);
}

// Define our responsive menu settings.
function genesis_child_responsive_menu_settings() {
	$settings = array(
		'mainMenu'          => __( 'Menu', 'genesis-child' ),
		'menuIconClass'     => 'dashicons-before dashicons-menu',
		'subMenu'           => __( 'Submenu', 'genesis-child' ),
		'subMenuIconsClass' => 'dashicons-before dashicons-arrow-down-alt2',
		'menuClasses'       => array(
			'combine' => array(
				'.nav-primary',
				'.nav-header',
			),
			'others'  => array(),
		),
	);
	return $settings;
}

// Add HTML5 markup structure.
add_theme_support( 'html5', array( 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ) );

// Add Accessibility support.
add_theme_support( 'genesis-accessibility', array( '404-page', 'drop-down-menu', 'headings', 'rems', 'search-form', 'skip-links' ) );

// Add viewport meta tag for mobile browsers.
add_theme_support( 'genesis-responsive-viewport' );

// Add support for custom header.
add_theme_support( 'custom-header', array(
	'width'           => 600,
	'height'          => 160,
	'header-selector' => '.site-title a',
	'header-text'     => false,
	'flex-height'     => true,
) );

// Add support for custom background.
add_theme_support( 'custom-background' );

// Add support for after entry widget.
add_theme_support( 'genesis-after-entry-widget-area' );

// Add support for 3-column footer widgets.
add_theme_support( 'genesis-footer-widgets', 3 );

// Add Image Sizes.
add_image_size( 'featured-image', 800, 450, TRUE );

// Rename primary and secondary navigation menus.
add_theme_support( 'genesis-menus', array( 'primary' => __( 'After Header Menu', 'genesis-child' ), 'secondary' => __( 'Footer Menu', 'genesis-child' ) ) );

// Reposition the secondary navigation menu.
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_footer', 'genesis_do_subnav', 5 );

// Reduce the secondary navigation menu to one level depth.
add_filter( 'wp_nav_menu_args', 'genesis_child_secondary_menu_args' );
function genesis_child_secondary_menu_args( $args ) {
	if ( 'secondary' != $args['theme_location'] ) {
		return $args;
	}
	$args['depth'] = 1;
	return $args;
}

// Modify size of the Gravatar in the author box.
add_filter( 'genesis_author_box_gravatar_size', 'genesis_child_author_box_gravatar' );
function genesis_child_author_box_gravatar( $size ) {
	return 90;
}

// Modify size of the Gravatar in the entry comments.
add_filter( 'genesis_comment_list_args', 'genesis_child_comments_gravatar' );
function genesis_child_comments_gravatar( $args ) {
	$args['avatar_size'] = 60;
	return $args;
}


// Simple Hooks Textarea CSS
add_action('admin_head', 'my_custom_fonts');
function my_custom_fonts() {
  echo '<style>
    .genesis_page_simplehooks .genesis-metaboxes textarea {
      	background-color:#272822;
		color:#e6db74;
		font-family: Consolas,Monaco,monospace;
    	unicode-bidi: embed;
		overflow:scroll;
		min-height: 400px;
		width:100%;
  		padding:1.25rem;
    }
    #genesis-theme-settings-scripts textarea {
		background-color:#272822;
		color:#e6db74;
		font-family: Consolas,Monaco,monospace;
    	unicode-bidi: embed;
		overflow:scroll;
		min-height: 600px;
		width:100%;
  		padding:1.25rem;
    }
  </style>';
}

// SVG Support
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


//* Redirect Attachment page
function myprefix_redirect_attachment_page() {
	if ( is_attachment() ) {
		global $post;
		if ( $post && $post->post_parent ) {
			wp_redirect( esc_url( get_permalink( $post->post_parent ) ), 301 );
			exit;
		} else {
			wp_redirect( esc_url( home_url( '/' ) ), 301 );
			exit;
		}
	}
}
add_action( 'template_redirect', 'myprefix_redirect_attachment_page' );

// Limit Random Password Character to 20
add_filter('random_password', 'modify_the_pass');
function modify_the_pass($pass) {
    $pass = substr($pass, 0, 20); 
    return $pass; 
}

// show admin bar only for admins and editors
if (!current_user_can('edit_posts')) {
	add_filter('show_admin_bar', '__return_false');
}