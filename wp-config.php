<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'fusion' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'J{1e2f|rJhIP(jlqL,`l)3bEB]W%>nQAyB1X;2|35v/;m]2B!or|cBS% meW<p:U' );
define( 'SECURE_AUTH_KEY',  'w{VmX#b}!}hC;<c, (4.Ip]<-JIyJ${Z?OG-plti<`gYo/&]wk~D[shE{egT}PBw' );
define( 'LOGGED_IN_KEY',    'n7Z#iS4~d6vq@w{j=& ,_i {ui*i|u.:Tiz]}OlDKa}eP|hMjgx~vN~^.DZ-V;hE' );
define( 'NONCE_KEY',        'YrtXNZx(EQ+0jnIv.532C3pt%.tV)Z_M,n7PL*7beyY Ih O/E 6xN$)l3.^MvE$' );
define( 'AUTH_SALT',        'C#fjR@3y,:V!U*B|4],_`/nJAi4!$Hc.2d)?x(ygG1y_^~($LOYm#Iq[UFdUHhkf' );
define( 'SECURE_AUTH_SALT', 'bH;]XhZz}cpf%WTi}^g/fkxGez:dH <KNe9H.v;duU[s+e)zen{P<dhb1tiu;t)p' );
define( 'LOGGED_IN_SALT',   'DixS6ur0&tbo)( 6;W0Fgd,[dJLKi9L{-0.Ht-flF[;bp;J/fOu@Y;D!J)r;(]OF' );
define( 'NONCE_SALT',       '(+=a^N+>juWEYI,~rUi,bj%:Yv4$j%5g*UWX:b[hdBJXLD}*U->G|4kHZtvD:K+K' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
